import numpy as np
import pandas as pd
import os
import re
import math
import seaborn as sns 
from test_machine import VServer
import pickle
import torch
from nlp import Dataset
import tensorflow as tf
from sklearn.utils import shuffle
from transformers import TFAutoModelForSequenceClassification
from transformers import AutoTokenizer
from datasets import load_dataset, load_metric
from transformers import TrainingArguments
from transformers import Trainer
from transformers import AutoModelForSequenceClassification
from transformers import TrainingArguments
from transformers import DataCollatorWithPadding
from transformers.file_utils import PaddingStrategy
from transformers.tokenization_utils_base import TruncationStrategy
from sklearn.model_selection import train_test_split
from transformers import RobertaConfig, RobertaModel
from transformers import RobertaTokenizer
import c19_rep.c19.representations as representations

from c19_rep.c19.representations.factorization import SVD
from c19_rep.c19.representations.statistical import Stat
from c19_rep.c19.representations.sent_trans import BERTTransformer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score


class Predict():
    def __init__(self, path):
        self.path = path
        self.data = []
        
        rep = SVD(1000, 256)
           
        #self.repr = rep.fit(pd.read_csv("./data/train.tsv", sep="\t")["text"].to_list())

    def get_model(self, pickle_path):
        model = pickle.load(open(pickle_path, "rb"))
        return model

    def predict(self, round_data):
        
        dict = {}
        for key in round_data.keys():
            #str = self.repr.transform(round_data[key])
            model = self.get_model("./model/SVD")
            repre = pickle.load(open("./model/rep2SVD","rb"))
            str = round_data[key]
            str = [str]
            #print(str)
            pred = repre.transform(str)
            pred = model.predict(pred)
            dict[key] = pred

        return dict

    def run(self):
        vs = VServer(self.path)
        cnt = 0
        while True:
            cnt += 1
            print(cnt)
            round_data = vs.get()
            #print(round_data)
            if len(round_data) == 0:
                break

            predictions = self.predict(round_data)
            vs.post(predictions=predictions)
        return vs.get_results()

if __name__ == "__main__":
    p = Predict("./data")
    print(p.run())



