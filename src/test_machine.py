import numpy as np
import pandas as pd
import os

class VServer():

    def __init__(self, path):
        self.counter = {}
        self.results = {}
        self.correct_predictions = {}
        self.path = path
        self.set_correct_predictions()

    def set_correct_predictions(self):
        for file in os.listdir(os.path.join(self.path, "test_pos")):
            self.correct_predictions[file[:-4]] = 1
            self.counter[file[:-4]] = 0
            self.results[file[:-4]] = 0

        for file in os.listdir(os.path.join(self.path, "test_neg")):
            self.correct_predictions[file[:-4]] = 0
            self.counter[file[:-4]] = 0
            self.results[file[:-4]] = 0


    def get(self):
        data = {}
        for file in os.listdir(os.path.join(self.path, "test_pos")):
            df = pd.read_csv(os.path.join(os.path.join(self.path, "test_pos"), file), sep="\t")
            if "date" in df.columns:
                df = df.sort_values(by=["date"])
            if self.counter[file[:-4]] < len(df["text"]) and self.counter[file[:-4]] != -1:
                data[file[:-4]] = df["text"][self.counter[file[:-4]]]
                self.counter[file[:-4]] += 1
        
        for file in os.listdir(os.path.join(self.path, "test_neg")):
            df = pd.read_csv(os.path.join(os.path.join(self.path, "test_neg"), file), sep="\t")
            if "date" in df.columns:
                df = df.sort_values(by=["date"])
            if self.counter[file[:-4]] < len(df["text"]) and self.counter[file[:-4]] != -1:
                data[file[:-4]] = df["text"][self.counter[file[:-4]]]
                self.counter[file[:-4]] += 1

        return data

    def post(self, predictions):
        #print(pred)
        for key in predictions.keys():
            if predictions[key] == 1 and self.results[key] == 0:
                self.results[key] = self.counter[key]
                self.counter[key] = -1
        
    def get_results(self):
        recall = 0
        precision = 0
        f1 = 0
        preds = 0
        relevant = 0

        for key in self.results.keys():
            if self.correct_predictions[key] == 1 and self.results[key] > 0:
                precision += 1
                recall += 1
            if self.correct_predictions[key] == 1:
                relevant += 1
            if self.results[key] > 0:
                preds += 1
        #precision = 0
        if preds > 0:
            precision /= preds
        #recall = 0
        if relevant > 0:
            recall /= relevant
        
        if (precision + recall) > 0:
            f1 = (2 * precision * recall) / (precision + recall)
        return {
            'precision' : precision,
            'recall' : recall,
            'f1' : f1
        }
