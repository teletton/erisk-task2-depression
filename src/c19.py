import pandas as pd
import numpy as np
import os
import c19_rep.c19.representations as representations

from c19_rep.c19.representations.factorization import SVD
from c19_rep.c19.representations.statistical import Stat
from c19_rep.c19.representations.sent_trans_mental import MentalBERTTransformer
from c19_rep.c19.representations.sent_trans import BERTTransformer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score
import pickle
import math

class C19:
    def __init__(self, path):
        self.path = path
    def preporces(self):
        df = pd.read_csv(self.path, sep="\t")
        list = []
        for i in range(len(df["text"])):
            print(i)
            


            if df["text"][i] != df["text"][i] or (len(df["text"][i]) - df["text"][i].count(' ')) == 0:
                list.append(i)
        df = df.drop(list)
        df.to_csv(self.path, sep="\t", index=False)
    def train(self):
        os.environ['CUDA_VISIBLE_DEVICES']="4"

        df = pd.read_csv(self.path, sep="\t")
        
        #df = df[:100]
        df = df.dropna()
        X_train = df["text"].to_list()
        
        #print(X_train)
        y_train = df["label"].to_list()
        
        X_test = ["Take simple daily precautions to help prevent the spread of respiratory illnesses like #COVID19. Learn how to protect yourself from coronavirus (COVID-19): https://t.co/uArGZTrH5L. https://t.co/biZTxtUKyK","The NBA is poised to restart this month. In March we reported on how the Utah Jazz got 58 coronavirus tests in a matter of hours at a time when U.S. testing was sluggish. https://t.co/I8YjjrNoTh https://t.co/o0Nk6gpyos","We just announced that the first participants in each age cohort have been dosed in the Phase 2 study of our mRNA vaccine (mRNA-1273) against novel coronavirus. Read more: https://t.co/woPlKz1bZC #mRNA https://t.co/9VGUoJu5cS"]
        y_test = [1,0,1]
        
        for (name,representation) in [("mental",MentalBERTTransformer)]:
            print("vleze")
            
            
            if name == "SVD":
                representation = representation(1000, 256)
            else:
                representation = representation()

            _filename = "./model/rep3" + str(name)
            representation.fit(X_train)
            train_representation = representation.transform(X_train)
    
            pickle.dump(representation, open(_filename, "wb"))
            clf = LogisticRegression(random_state=0).fit(train_representation, y_train)
            pickle.dump(clf, open(str("./model/3" + name), "wb"))
        

            
            
            
if __name__ == "__main__":
    c = C19("./data/trainall.tsv")
    c.train()
