import numpy as np
import pandas as pd
import os
import re
import math
import seaborn as sns 
from test_machine import VServer
import pickle
import torch
from nlp import Dataset
import tensorflow as tf
from sklearn.utils import shuffle
from transformers import TFAutoModelForSequenceClassification
from transformers import AutoTokenizer
from datasets import load_dataset, load_metric
from transformers import TrainingArguments
from transformers import Trainer
from transformers import AutoModelForSequenceClassification
from transformers import TrainingArguments
from transformers import DataCollatorWithPadding
from transformers.file_utils import PaddingStrategy
from transformers.tokenization_utils_base import TruncationStrategy
from sklearn.model_selection import train_test_split
from transformers import RobertaConfig, RobertaModel
from transformers import RobertaTokenizer
import c19_rep.c19.representations as representations

from c19_rep.c19.representations.factorization import SVD
from c19_rep.c19.representations.statistical import Stat
from c19_rep.c19.representations.sent_trans import BERTTransformer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score
import requests
import json


class Predict1():
    def __init__(self, path):
        self.path = path
        self.data = {}
        self.data1 = {}
        self.data2 = {}
        

    def get_model(self, pickle_path):
        model = pickle.load(open(pickle_path, "rb"))
        return model

    def get_model1(self, pickle_path):
        model = pickle.load(open(pickle_path, "rb"))
        return model

    def predict1(self, round_data):
        
        dict = []
        for key in round_data.keys():
            #str = self.repr.transform(round_data[key])
            model = self.get_model1("./model/3mental")
            repre = pickle.load(open("./model/rep3mental","rb"))
            str = round_data[key]
            str = [str]
            #print(str)
            pred = repre.transform(str)
            pred = model.predict(pred)
            self.data1[key] = pred[0]
            dict.append({
                "nick" : key,
                "decision" : int(pred[0]),
                "score" : int(pred[0])
            })
            
        for key in self.data1.keys():
            if key not in round_data.keys():
                dict.append({
                "nick" : key,
                "decision" : int(self.data1[key]),
                "score" : int(self.data1[key])
            })
        return dict

    def predict(self, round_data):
        
        dict = []
        for key in round_data.keys():
            #str = self.repr.transform(round_data[key])
            model = self.get_model("./model/3sen_bert")
            repre = pickle.load(open("./model/rep3sen_bert","rb"))
            str = round_data[key]
            str = [str]
            #print(str)
            pred = repre.transform(str)
            pred = model.predict(pred)
            self.data[key] = pred[0]
            dict.append({
                "nick" : key,
                "decision" : int(pred[0]),
                "score" : int(pred[0])
            })
            
        for key in self.data.keys():
            if key not in round_data.keys():
                dict.append({
                "nick" : key,
                "decision" : int(self.data[key]),
                "score" : int(self.data[key])
            })
        return dict

    def run(self):
        vs = VServer(self.path)

        cnt = 0
        while True:
            cnt += 1
            print(cnt)
            round_data = requests.get("https://erisk.irlab.org/challenge-t2/getwritings/RK4J0RmsL8Tb7LACkDQ2BC4loQFwtrHKmn79P77iAJs")
            rd = {}
            cnt1 = 0
            predictions3 = []
            for data in round_data.json():
                if cnt1 == 0:
                    print(data["date"])
                cnt1 += 1
                rd[data["nick"]] = str(data["content"])
                self.data2[data["nick"]] = 0
                predictions3.append({
                    "nick" : data["nick"],
                    "decision" : int(0),
                    "score" : int(0)
                })
            print(cnt1)
            
            
            if len(rd) == 0:
                break
            for key in self.data2.keys():
                if key not in rd.keys():
                    predictions3.append({
                        "nick" : key,
                        "decision" : int(0),
                        "score" : int(0)
                    })

            predictions = self.predict(rd)
            
            r = requests.post("https://erisk.irlab.org/challenge-t2/submit/RK4J0RmsL8Tb7LACkDQ2BC4loQFwtrHKmn79P77iAJs/1", json=predictions)
            print(r.status_code)
            r = requests.post("https://erisk.irlab.org/challenge-t2/submit/RK4J0RmsL8Tb7LACkDQ2BC4loQFwtrHKmn79P77iAJs/2", json=predictions3)
            print(r.status_code)
            r = requests.post("https://erisk.irlab.org/challenge-t2/submit/RK4J0RmsL8Tb7LACkDQ2BC4loQFwtrHKmn79P77iAJs/0", json=predictions3)
            print(r.status_code)
            r = requests.post("https://erisk.irlab.org/challenge-t2/submit/RK4J0RmsL8Tb7LACkDQ2BC4loQFwtrHKmn79P77iAJs/3", json=predictions3)
            print(r.status_code)
            r = requests.post("https://erisk.irlab.org/challenge-t2/submit/RK4J0RmsL8Tb7LACkDQ2BC4loQFwtrHKmn79P77iAJs/4", json=predictions3)
            print(r.status_code)

            
        return 0

if __name__ == "__main__":
    p = Predict1("./data")
    print(p.run())



