from operator import index
from random import shuffle
import numpy as np
from sklearn.utils import shuffle
import pandas as pd
import os
import re
import math
import seaborn as sns 
from train import Train

if __name__ == "__main__":
    pos = 3
    neg = 0
    all = 0
    pos_wri = 90 + 1061 + 746
    neg_wri = 0
    all_wri = 0
    for file in os.listdir(os.path.join("./data/", "train_neg")):

        neg += 1
        all += 1
        df = pd.read_csv(os.path.join(os.path.join("./data/", "train_neg"), file), sep="\t")
        neg_wri += len(df)

    for file in os.listdir(os.path.join("./data/", "train_pos")):
        print(file)
        pos += 1
        all += 1
        df = pd.read_csv(os.path.join(os.path.join("./data/", "train_pos"), file), sep="\t")
        pos_wri += len(df)
        
    
        
        
    print(pos)
    print(neg)
    print(pos + neg)
    print(pos / (pos + neg))
    print(neg / (pos + neg))
    print(pos_wri)
    print(neg_wri)
    print(pos_wri + neg_wri)
    print(pos_wri / (pos_wri + neg_wri))
    print(neg_wri / (pos_wri + neg_wri))

