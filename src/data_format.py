import numpy as np
import pandas as pd
from os import listdir
import xml.etree.ElementTree as Xet

if __name__ == "__main__":
    niz1 = listdir("./data/training_t2/TRAINING_DATA/2017_cases/neg")
    niz2 = listdir("./data/training_t2/TRAINING_DATA/2018_cases/neg")
    
    niz = []
    for st in niz1:
        niz.append("./data/training_t2/TRAINING_DATA/2017_cases/neg/" + st)
    for st in niz2:
        niz.append("./data/training_t2/TRAINING_DATA/2018_cases/neg/" + st)
    
    train = niz[:int(len(niz)*0.8)]
    test = niz[int(len(niz)*0.8):]
    cols = ["title", "date", "text"]
    
    for file in train:
        rows = []
        xmlparse = Xet.parse(file)
        root = xmlparse.getroot()
        for i in range(1, len(root)):
            print(root[i])
            print(root[i][0].text)
            print(root[i][1].text)
            print(root[i][2].text)
            print(root[i][3].text)
            rows.append({'title' : root[i][0].text, 'date' : root[i][1].text, 'text' : root[i][3].text})

        train_df = pd.DataFrame(rows, columns=cols)
        if len(file.split("_")) > 4:
            train_df.to_csv("./data/train_neg/" + file.split("_")[len(file.split("_")) - 1][:-4] + ".tsv", index=False, sep='\t', encoding="utf-8")
        else:
            train_df.to_csv("./data/train_neg/" + file.split("/")[len(file.split("/")) - 1][:-4] + ".tsv", index=False, sep='\t', encoding="utf-8")
        