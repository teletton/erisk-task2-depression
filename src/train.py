import numpy as np
import pandas as pd
import os
import re
import math
 
from nlp import Dataset
import tensorflow as tf
from sklearn.utils import shuffle
from transformers import TFAutoModelForSequenceClassification
from transformers import AutoTokenizer
from datasets import load_dataset, load_metric
from transformers import TrainingArguments
from transformers import Trainer
from transformers import AutoModelForSequenceClassification
from transformers import TrainingArguments
from transformers import DataCollatorWithPadding
from transformers.file_utils import PaddingStrategy
from transformers.tokenization_utils_base import TruncationStrategy
from sklearn.model_selection import train_test_split
from transformers import RobertaConfig, RobertaModel
from transformers import RobertaTokenizer
from transformers import DistilBertModel, DistilBertTokenizer
import torch

os.environ['CUDA_VISIBLE_DEVICES']="4"
def preprocess_function(examples):
        tokenizer = DistilBertTokenizer.from_pretrained("distilbert-base-uncased")
        return tokenizer(examples["text"], truncation=True)


class Train:
    def __init__(self, path):
        self.path = path
        self.train = pd.DataFrame(columns=["text", "label"])
    
    def preprocess(self):
        for file in os.listdir(self.path):
            print(len(self.train))
            df = pd.read_csv(os.path.join(self.path, file), sep="\t")
            
            df = df[["text", "label"]]
            self.train = pd.concat([self.train, df], axis=0)
        self.train.to_csv("./data/train/train.tsv", sep="\t", index=False)
        print(self.train)
    
    def set_class(self):
        for file in os.listdir(os.path.join(self.path, "train_pos")):
            print(file)
            df = pd.read_csv(os.path.join(os.path.join(self.path, "train_pos"), file), sep='\t')
            print(df.columns[0])
            df["label"] = [1 for i in range(len(df))]
            print(df["date"])
            df = df.sort_values(by=["date"])
            df = df[["text", "label"]]
            break
            #df.to_csv(os.path.join(os.path.join(self.path, "train_pos"), file), sep='\t', index=False)
            
            

        """
        for file in os.listdir(os.path.join(self.path, "train_neg")):
            df = pd.read_csv(os.path.join(os.path.join(self.path, "train_neg"), file), sep='\t')
            df["label"] = [1 for i in range(len(df))]
            df.to_csv(os.path.join(os.path.join(self.path, "train_neg"), file), sep='\t', index=False)
        """

    def provide_data(self):
        print("vleze")
        for file in os.listdir(self.path):
            print(file)
            text = ""
            df = pd.read_csv(os.path.join(self.path, file), sep = "\t")
            _class = df["class"][0]
            for i in range(len(df["text"])):
                text += df["text"][i]
                dict = {
                    "text" : [text],
                    "class" : [_class]
                }
                sdf = pd.DataFrame(dict)
                sds = Dataset.from_pandas(sdf[["text", "class"]])
                tokenized = sds.map(preprocess_function)
                tokenized.drop('text')
                print(tokenized)
                yield tokenized

    def build_model(self):
        tokenizer = DistilBertTokenizer.from_pretrained("distilbert-base-uncased")
        data_collator = DataCollatorWithPadding(tokenizer=tokenizer)
        model = DistilBertModel.from_pretrained("distilbert-base-uncased", num_labels=2)
        train_df = pd.read_csv("./data/train.tsv", sep="\t")
        train_df = train_df[:100]
        train_ds = Dataset.from_pandas(train_df)
        train_token = train_ds.map(preprocess_function)
        train_token.drop('text')

        training_args = TrainingArguments(
            output_dir='./results',
            per_device_train_batch_size=64,
            num_train_epochs=1,
        )
        
        trainer = Trainer(
            model=model,
            args=training_args,
            train_dataset=train_token,  
            tokenizer=tokenizer,
            data_collator=data_collator,
        )
        torch.save(trainer, "./model/")
        trainer.save_model("./model/mod.pickle")

    
    def run(self):
        trainer = self.build_model()
        trainer.train()
       
        

if __name__ == "__main__":
    t = Train("./data")
    t.build_model()
