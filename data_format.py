import numpy as np
import pandas as pd
from os import listdir
import os
import xml.etree.ElementTree as Xet

if __name__ == "__main__":
    for file in os.listdir("./data/train_neg"):
        print(file)
        df = pd.read_csv("./data/train_neg/" + file, sep="\t")
        df = df.sort_values(by=["date"])
        list = [0 for i in range(len(df))]
        df["class"] = list
        df.to_csv("./data/train/" + file, sep="\t", index=False)
    
    for file in os.listdir("./data/train_pos"):
        print(file)
        df = pd.read_csv("./data/train_pos/" + file, sep="\t")
        df = df.sort_values(by=["date"])
        list = [1 for i in range(len(df))]
        df["class"] = list
        df.to_csv("./data/train/" + file, sep="\t", index=False)

